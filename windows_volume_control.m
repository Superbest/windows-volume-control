function windows_volume_control
% Generates a registry file which sets the windows volume control slider's
% response curve to an arbitrary (numeric) function.
%
% Windows allows supplying of a custom curve to the volume control,
% which is useful if the sensitivity is not appropriate for the
% speakers or headphones in use.
%
% Adapted from http://www.herethere.net/~samson/php/volume_control/

%% Parameters and constants

make_default = true;



raw = { ...
    '00', '00', '00', '00', '54', '00', '00', '00', '5e', '00', '00', '00', ...
    '68', '00', '00', '00', '74', '00', '00', '00', '81', '00', '00', '00', ...
    '90', '00', '00', '00', 'a0', '00', '00', '00', 'b3', '00', '00', '00', ...
    'c7', '00', '00', '00', 'dd', '00', '00', '00', 'f7', '00', '00', '00', ...
    '12', '01', '00', '00', '32', '01', '00', '00', '54', '01', '00', '00', ...
    '7b', '01', '00', '00', 'a6', '01', '00', '00', 'd6', '01', '00', '00', ...
    '0b', '02', '00', '00', '46', '02', '00', '00', '88', '02', '00', '00', ...
    'd2', '02', '00', '00', '24', '03', '00', '00', '7f', '03', '00', '00', ...
    'e4', '03', '00', '00', '55', '04', '00', '00', 'd3', '04', '00', '00', ...
    '5f', '05', '00', '00', 'fb', '05', '00', '00', 'a8', '06', '00', '00', ...
    '6a', '07', '00', '00', '41', '08', '00', '00', '30', '09', '00', '00', ...
    '3b', '0a', '00', '00', '63', '0b', '00', '00', 'ae', '0c', '00', '00', ...
    '1d', '0e', '00', '00', 'b7', '0f', '00', '00', '7f', '11', '00', '00', ...
    '7a', '13', '00', '00', 'af', '15', '00', '00', '24', '18', '00', '00', ...
    'e0', '1a', '00', '00', 'ec', '1d', '00', '00', '50', '21', '00', '00', ...
    '16', '25', '00', '00', '4a', '29', '00', '00', 'f7', '2d', '00', '00', ...
    '2d', '33', '00', '00', 'f9', '38', '00', '00', '6d', '3f', '00', '00', ...
    '9d', '46', '00', '00', '9d', '4e', '00', '00', '85', '57', '00', '00', ...
    '70', '61', '00', '00', '7a', '6c', '00', '00', 'c4', '78', '00', '00', ...
    '73', '86', '00', '00', 'af', '95', '00', '00', 'a4', 'a6', '00', '00', ...
    '86', 'b9', '00', '00', '8b', 'ce', '00', '00', 'f1', 'e5', '00', '00', ...
    'ff', 'ff', '00', '00'};

% plot_curve(raw);

%% Gets the appropriate array of strings

if make_default
    contents = get_default_file;    
else
    
end

%% Save the registry file to the same directory as the same script

cd(fileparts(mfilename('fullpath')));

f = fopen('Volume curve.reg', 'w');

for i = 1:length(contents)
    fprintf(f, '%s\n', contents{i});
end

fclose(f);

    function plot_curve(raw_bytes)
        % Takes a sequence of numerical values, assuming a specific format, and
        % plots the function they describe.
        %
        % The format is assumed to be the following: The array is made up of
        % two-character strings, each one a hexadecimal byte (byte means 0-256
        % inclusive in this case). Every quadruplet of strings comes together
        % to make up one 8-digit hexadecimal number. The numbers make up an
        % ordered vector of y values.
        
        filtered = reshape(raw_bytes, 4, length(raw_bytes)/4)';
        
        hex = cell(size(filtered, 1), 1);
        for i = 1:size(filtered, 1)
            hex{i} = [filtered{i, 2} filtered{i, 3}];
        end
        
        dec = hex2dec(hex);
        
        plot(dec);
    end

    function reg_string = make_reg(raw_bytes)
        prefix = ['[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Multime' ...
            'dia\Audio\VolumeControl]\r"EnableVolumeTable"=' ...
            'dword:00000000\r"VolumeTable"=hex:'];
        
        def_bytes = {
            '00', '00', '00', '00', '0f', '04', '00', '00', ...
            'd2', '04', '00', '00', 'bb', '05', '00', '00', ...
            'd0', '06', '00', '00', '18', '08', '00', '00', ...
            '9f', '09', '00', '00', '6f', '0b', '00', '00', ...
            '97', '0d', '00', '00', '27', '10', '00', '00', ...
            '32', '13', '00', '00', 'd1', '16', '00', '00', ...
            '1e', '1b', '00', '00', '3a', '20', '00', '00', ...
            '4e', '26', '00', '00', '86', '2d', '00', '00', ...
            '1b', '36', '00', '00', '4e', '40', '00', '00', ...
            '6d', '4c', '00', '00', 'd5', '5a', '00', '00', ...
            'f4', '6b', '00', '00', '4d', '80', '00', '00', ...
            '7d', '98', '00', '00', '3b', 'b5', '00', '00', ...
            '65', 'd7', '00', '00', 'ff', 'ff', '00', '00'
            };
        
        reg_string = 'Nope';
    end

    function def_file = get_default_file
        def_file = {
            'Windows Registry Editor Version 5.00'
            ''
            '[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Multimedia\Audio\VolumeControl]'
            '"EnableVolumeTable"=dword:00000000'
            '"VolumeTable"=hex:00,00,00,00,0f,04,00,00,d2,04,00,00,bb,05,00,00,d0,06,00,00,\'
            '18,08,00,00,9f,09,00,00,6f,0b,00,00,97,0d,00,00,27,10,00,00,32,13,00,00,d1,\'
            '16,00,00,1e,1b,00,00,3a,20,00,00,4e,26,00,00,86,2d,00,00,1b,36,00,00,4e,40,\'
            '00,00,6d,4c,00,00,d5,5a,00,00,f4,6b,00,00,4d,80,00,00,7d,98,00,00,3b,b5,00,\'
            '00,65,d7,00,00,ff,ff,00,00'
            };
    end


end